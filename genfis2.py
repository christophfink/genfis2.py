#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__all__ = [
    "genfis2"
]

import math
import numpy

from subclust import subclust


def genfis2(
    xIn,
    xOut,
    radii,
    xBounds=None,
    squashFactor=1.25,
    acceptRatio=0.5,
    rejectRatio=0.15,
    verbose=False
):
    """
        Generates Sugeno-type FIS using substractive clustering

        Args::
            xIn (iterable of iterables of float)
                    matrix of  FIS input columns.
            xOut (iterable of iterables of float)
                    matrix of FIS output columns.
            radii (float)
                    range of influence of the cluster centres,
                    range [0…1], smaller values result in more rules.
            xBounds (iterable of float)
                    if specified: normalise the xIn and xOut values by
                    the supplied coefficients (one per xIn/xOut column).
                    Default: None
            squashFactor (float):
                    squash factor as accepted by subclust().
                    Default: 1.25
            acceptRatio (float):
                    acceptance ratio, as accepted by subclust().
                    Default: 0.5
            rejectRatio (float):
                    rejectance ratio, as accept by subclust().
                    Default: 0.15
            verbose (boolean):
                    make subclust() print progress information.
                    Default: False

        Returns::
            (iterable): matrix of FIS rules

    """
    xIn = numpy.array(xIn)
    xOut = numpy.array(xOut)

    numData, numInp = xIn.shape
    numData2, numOutp = xOut.shape

    if numData != numData2:
        raise ValueError("input and output dimensions do not match")

    centers, sigmas = subclust(
        numpy.concatenate(
            (xIn, xOut),
            axis=1
        ),
        radii,
        xBounds,
        squashFactor,
        acceptRatio,
        rejectRatio,
        verbose
    )

    centers = centers[:, :numInp]
    sigmas = sigmas[:numInp]

    distMultp = (1.0 / math.sqrt(2.0)) / sigmas

    numRule, _ = centers.shape

    sumMu = numpy.zeros((numData, 1))
    muVals = numpy.zeros((numData, 1))
    dxMatrix = numpy.zeros((numData, numInp))
    muMatrix = numpy.zeros((numData, numRule * (numInp + 1)))

    for i in range(numRule):
        for j in range(numInp):
            dxMatrix[:, j] = (xIn[:, j] - centers[i, j]) * distMultp[j]

        dxMatrix *= dxMatrix

        if numInp > 1:
            muVals = numpy.exp(-1 * numpy.sum(dxMatrix, axis=0))
        else:
            muVals = numpy.exp(-1 * dxMatrix)

        sumMu += muVals

        colNum = (i - 1) * (numInp + 1)
        for j in range(numInp):
            muMatrix[:, colNum + numInp + 1] = xIn[:, j] * muVals[:, j]

        muMatrix[:, colNum + numInp + 1] = muVals[:, j]

    outEqns = numpy.linalg.lstsq(muMatrix, xOut, rcond=None)

    # numInDigit = math.floor(math.log10(numInp)) + 1
    # numOutDigit = math.floor(math.log10(numOutp)) +1
    # numRuleDigit = math.floor(math.log10(numRule)) + 1

    # numRow=11 + (2 * (numInp + numOutp)) + (3 * (numInp + numOutp) * numRule)
    # numCol = numInp + numOutp + 2
    # strSize = 3 + numInDigit + numOutDigit

    print(outEqns)


if __name__ == "__main__":
    pass
